%{!?_release: %define _release 0}
%{!?_version: %define _version 0.0.0}

%define product azkaban
%define service workflow

Name:             %{product}
Summary:          3rd-party: Azkaban Workflow
Version:          %{_version}
Release:          %{_release}
License:          (c) 2017 Thumbtack Technology, LLC
Group:            3rd-party/Azkaban
Vendor:           Thumbtack Technology, LLC
URL:              http://thumbtack.net/skills/dataswitch.html
Packager:         Thumbtack DevOps Team, <devops-team@thumbtack.net>
BuildArch:        noarch
Requires(pre):    /usr/sbin/useradd, /usr/bin/getent
Requires(postun): /usr/sbin/userdel
Prefix:           /opt/%{product}

%define __jar_repack %{nil}

# Define home, log and data directories as well as
# an owner for installed services
%define home_executor  /opt/%{product}-executor
%define home_web  /opt/%{product}-web
%define logs_executor  %{_localstatedir}/log/%{product}-executor
%define logs_web  %{_localstatedir}/log/%{product}-web

%define owner %{product}

%description
azkaban-workflow

%package executor
Summary:          This package provides Azkaban executor
%description executor
  This package provides Azkaban executor.

%package web
Summary:          This package provides Azkaban WebUI
%description web
  This package provides Azkaban web.

%prep
# Clean all old content from the BUILD directory
rm -rf *
# Copy source code
rsync --delete -a --exclude="$(realpath --relative-to=%{_sourcedir} %{_topdir})" ${RPM_SOURCE_DIR}/azkaban/azkaban-exec-server ./
rsync --delete -a --exclude="$(realpath --relative-to=%{_sourcedir} %{_topdir})" ${RPM_SOURCE_DIR}/azkaban/azkaban-web-server ./
rsync --delete -a --exclude=${RPM_SOURCE_DIR}/azkaban --exclude="$(realpath --relative-to=%{_sourcedir} %{_topdir})" ${RPM_SOURCE_DIR}/rpm ./


%build
# Azkaban-executor
export AZ_SVC_OWNER=%{owner} AZ_SVC_NAME=%{product}-executor
export AZ_SVC_HOME=%{home_executor} AZ_SVC_LOGS=%{logs_executor}
envsubst '${AZ_SVC_OWNER},${AZ_SVC_HOME},${AZ_SVC_NAME},${AZ_SVC_LOGS}' < rpm/systemd.service.tp > %{product}-executor.service
envsubst '${AZ_SVC_HOME},${AZ_SVC_NAME},${AZ_SVC_LOGS}' < rpm/log4j.properties > log4j.properties

# Azkaban-web
export AZ_SVC_OWNER=%{owner} AZ_SVC_NAME=%{product}-web
export AZ_SVC_HOME=%{home_web} AZ_SVC_LOGS=%{logs_web}
envsubst '${AZ_SVC_OWNER},${AZ_SVC_HOME},${AZ_SVC_NAME},${AZ_SVC_LOGS}' < rpm/systemd.service.tp > %{product}-web.service
envsubst '${AZ_SVC_HOME},${AZ_SVC_NAME},${AZ_SVC_LOGS}' < rpm/log4j.properties > log4j.properties

%install
rm -rf %{buildroot}

# Azkaban-executor
# Create home and log directories
%{__install} -m 755 -d %{buildroot}/%{home_executor}/{bin,lib,conf/templates,extlib,plugins} %{buildroot}/%{logs_executor}
%{__ln_s} -f %{logs_executor} %{buildroot}/%{home_executor}/log 

# Install  binaries, libraries and configuration files
cp azkaban-exec-server/build/install/azkaban-exec-server/bin/* %{buildroot}/%{home_executor}/bin/
cp azkaban-exec-server/build/install/azkaban-exec-server/lib/* %{buildroot}/%{home_executor}/lib/
cp rpm/lib/* %{buildroot}/%{home_executor}/lib/
%{__install} -m 644 -D rpm/azkaban/executor/azkaban.properties %{buildroot}/%{home_executor}/conf/
%{__install} -m 644 -D rpm/azkaban/executor/azkaban.properties.j2 %{buildroot}/%{home_executor}/conf/templates/
%{__install} -m 644 -D rpm/global.properties %{buildroot}/%{home_executor}/conf/
%{__install} -m 644 -D %{product}-executor.service %{buildroot}%{_prefix}/lib/systemd/system/%{product}-executor.service
%{__install} -m 664 -D rpm/log4j.properties %{buildroot}/%{home_executor}/conf/log4j.properties

# Azkaban-web
# Create home and log directories
%{__install} -m 755 -d %{buildroot}/%{home_web}/{bin,lib,conf/templates,extlib,plugins,web} %{buildroot}/%{logs_web}
%{__ln_s} -f %{logs_web} %{buildroot}/%{home_web}/log

# Install  binaries, libraries and configuration files
cp azkaban-web-server/build/install/azkaban-web-server/bin/* %{buildroot}/%{home_web}/bin/
cp azkaban-web-server/build/install/azkaban-web-server/lib/* %{buildroot}/%{home_web}/lib/
cp rpm/lib/* %{buildroot}/%{home_web}/lib/
cp -r azkaban-web-server/build/install/azkaban-web-server/web/* %{buildroot}/%{home_web}/web/
%{__install} -m 644 -D rpm/azkaban/web/azkaban-users.xml %{buildroot}/%{home_web}/conf/
%{__install} -m 644 -D rpm/azkaban/web/azkaban-users.xml.j2 %{buildroot}/%{home_web}/conf/templates/
%{__install} -m 644 -D rpm/azkaban/web/azkaban.properties %{buildroot}/%{home_web}/conf/
%{__install} -m 644 -D rpm/azkaban/web/azkaban.properties.j2 %{buildroot}/%{home_web}/conf/templates/
%{__install} -m 644 -D rpm/global.properties %{buildroot}/%{home_web}/conf/
%{__install} -m 644 -D %{product}-web.service %{buildroot}%{_prefix}/lib/systemd/system/%{product}-web.service
%{__install} -m 664 -D rpm/log4j.properties %{buildroot}/%{home_web}/conf/log4j.properties

# Azkaban-executor
%files executor
%defattr(-,%{owner},%{owner},-)
%{home_executor}
%{logs_executor}
%defattr(-,root,root,-)
%{_prefix}/lib/systemd/system/%{product}-executor.service

# Azkaban-web
%files web
%defattr(-,%{owner},%{owner},-)
%{home_web}
%{logs_web}
%defattr(-,root,root,-)
%{_prefix}/lib/systemd/system/%{product}-web.service

# Azkaban-executor
%pre executor 
getent group %{owner} >/dev/null || groupadd -r %{owner}
getent passwd %{owner} >/dev/null || useradd -r -g %{owner} %{owner} -s /sbin/nologin >/dev/null

# Azkaban-web
%pre web
getent group %{owner} >/dev/null || groupadd -r %{owner}
getent passwd %{owner} >/dev/null || useradd -r -g %{owner} %{owner} -s /sbin/nologin >/dev/null

# Azkaban-executor
%post executor
systemctl enable %{product}-executor

# Azkaban-web
%post web
systemctl enable %{product}-web

# Azkaban-executor
%preun executor
systemctl disable %{product}-executor
systemctl stop %{product}-executor

# Azkaban-web
%preun web
systemctl disable %{product}-web
systemctl stop %{product}-web

%postun
#TODO: Need to understand this behaviour when you upgrade pacckage
#      It may be a reason why owner disappears while upgrading package
#/usr/sbin/userdel %{owner}

%clean
rm -rf *

#%changelog


